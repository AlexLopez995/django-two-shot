from django.shortcuts import render, redirect
from receipts.models import Receipt, ExpenseCategory, Account
from receipts.forms import ReceiptForm, ExpenseCategoryForm, AccountForm
from django.contrib.auth.decorators import login_required

@login_required
def receipts_view(request):
    receipts_view = Receipt.objects.filter(purchaser=request.user)
    context = {
        "receipts_view": receipts_view,
    }
    return render(request, "receipts/list.html", context)

@login_required
def new_receipt(request):
    if request.method == "POST":
        form = ReceiptForm(request.POST)
        if form.is_valid():
            new_receipt = form.save(False)
            new_receipt.purchaser = request.user
            new_receipt.save()
        return redirect("home")
    else:
        form = ReceiptForm()
    context = {
        "form": form
    }
    return render(request, 'receipts/create.html', context)

@login_required
def category_list(request):
    category_list = ExpenseCategory.objects.filter(owner=request.user)
    context = {
        "category_list": category_list,
    }
    return render(request, "receipts/categories.html", context)

@login_required
def account_list(request):
    account_list = Account.objects.filter(owner=request.user)
    context = {
        "account_list": account_list,
    }
    return render(request, "receipts/accounts_list_view.html", context)

@login_required
def create_category(request):
    if request.method == "POST":
        form = ExpenseCategoryForm(request.POST)
        if form.is_valid():
            category_name = form.save(commit=False)
            category_name.owner = request.user
            category_name.save()
            return redirect("category_list")
    else:
        form = ExpenseCategoryForm()
    context = {
        "form": form
    }
    return render(request, "categories/expense.html", context)

@login_required
def create_account(request):
    if request.method == "POST":
        form = AccountForm(request.POST)
        if form.is_valid():
            create_account = form.save(commit=False)
            create_account.owner = request.user
            create_account.save()
            return redirect("account_list")
    else:
        form = AccountForm()
    context = {
        "form": form
    }
    return render(request, "accounts/final.html", context)
